'use strict';

const express = require('express');
const router = express.Router();
const domainsService = require('./domains-service');
const eventTypesService = require('./eventTypes-service');

router.get('/', function (req, res) {
    domainsService.getAll((err, domains) => {
            if (err) {
                res.status(500).send({
                    msg: err
                });
            } else if (domains !== null) {
                res.status(200).send(domains);
            }
        }
    );
});

router.get('/:domainName', async (req, res) => {
    let domainName = req.params.domainName;
    let domain = await domainsService.getByName(domainName);

    if (domain.length === 0) {
        res.status(404).send(domain);
    } else if (domain !== null) {
        res.status(200).send(domain);
    }
});

router.post('/', function (req, res) {
    let domainToAdd = req.body;
    domainToAdd.eventTypes = [];

    domainsService.add(domainToAdd, (err, response) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (!('upserted' in response.result)) {
            res.status(409).send({
                msg: 'A domain with that name already exists'
            });
        } else {
            res.status(201).send({msg: 'Domain created'});
        }
    });
});

router.put('/:_id', function (req, res) {
    const _id = req.params._id;
    const updatedDomain = req.body;

    domainsService.update(_id, updatedDomain, (err, numUpdates) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (numUpdates.result.n === 0) {
            res.status(404).send({
                msg: 'Domain does not exists'
            });
        } else {
            res.status(200).send({
                msg: 'Domain updated'
            });
        }
    });
});

router.put('/domain/:domainName/eventType/:eventTypeId', function (req, res) {
    const domainName = req.params.domainName;
    const eventTypeInserted = req.params.eventTypeId;

    domainsService.addEventTypeByName(domainName, eventTypeInserted, (err, numUpdates) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (numUpdates.result.n === 0) {
            res.status(404).send({
                msg: 'There is not a domain with the name: ' + domainName
            });
        } else {
            res.status(201).send({
                msg: 'New simple eventType added!'
            });
        }
    });
});

router.delete('/', function (req, res) {
    domainsService.removeAll((err) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else {
            res.status(200).send({msg: 'Domains removed'});
        }
    });
});

router.delete('/:domainName', async (req, res) => {
    let domainName = req.params.domainName;
    let domain = await domainsService.getByName(domainName);

    if (domain.length > 0) {
        domainsService.remove(domain[0]._id);

        let eventTypes = await eventTypesService.getIds(domain[0].eventTypes);
        let deploymentIds = [];
        let patterns = [];

        for (const eT of eventTypes) {
            let removedEventType = await eventTypesService.removeEventType(eT, domainName);
            deploymentIds = deploymentIds.concat(removedEventType.deploymentId);
            patterns = patterns.concat(removedEventType.patterns);
        }


        res.status(200).send({
            msg: 'Domain removed',
            deploymentIds: [...new Set(deploymentIds.filter(d => d !== "null"))],
            patterns: patterns
        });
    } else {
        res.status(404).send({
            msg: 'Domain not found',
            deploymentIds: []
        });
    }
});

module.exports = router;

/**** FOR TESTING PURPOSES ***
 *
 {
    "_id": {
        "$oid": "5cbee0a81bbe137d7051745d"
    },
    "name": "Test",
    "description": "",
    "eventTypes": [
        {
            "$oid": "5cbee0b73f873d3848b63291"
        }
    ]
}

 {
    "_id": {
        "$oid": "5cbee0b73f873d3848b63291"
    },
    "timestamp": "1556011470512",
    "schema": {
        "name": "Aaaa",
        "type": "record",
        "fields": [
            {
                "name": "eeee",
                "type": "string"
            }
        ]
    },
    "deploymentId": "evento",
    "patternIds": [
        {
            "$oid": "5cbee73dc4c23d3310eeadc3"
        },
        {
            "$oid": "5cbef822ba52c027f44e9c26"
        }
    ]
}

 {
    "_id": {
        "$oid": "5cbee73dc4c23d3310eeadc3"
    },
    "name": "Alert",
    "pattern": "@public @Name(\"Alert\")  @Tag(name=\"domainName\", value=\"Test\")  insert into Alert  select a1.* from AAA a1",
    "timestamp": "1556011619402",
    "dependencies": {
        "eventTypeIds": [
            {
                "$oid": "5cbee0b73f873d3848b63291"
            }
        ],
        "patternIds": []
    },
    "deploymentId": "patroncito",
    "requirements": {
        "eventTypeIds": [],
        "patternIds": [
            {
                "$oid": "5cbef822ba52c027f44e9c26"
            }
        ]
    }
}

 {
    "_id": {
        "$oid": "5cbef822ba52c027f44e9c26"
    },
    "name": "Alert",
    "pattern": "@public @Name(\"Alert\")  @Tag(name=\"domainName\", value=\"Test\")  insert into Alert  select a1.* from AAA a1",
    "timestamp": "1556011619402",
    "dependencies": {
        "eventTypeIds": [
            {
                "$oid": "5cbee0b73f873d3848b63291"
            }
        ],
        "patternIds": [
            {
                "$oid": "5cbee73dc4c23d3310eeadc3"
            }
        ]
    },
    "deploymentId": "patron2",
    "requirements": {
        "eventTypeIds": [],
        "patternIds": []
    }
}
 */


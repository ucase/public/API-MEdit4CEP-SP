'use strict';

const MongoClient = require('mongodb').MongoClient;
let db;
let ObjectId = require('mongodb').ObjectID;
const Patterns = function () {
};

Patterns.prototype.connectDb = function (callback) {
    MongoClient.connect(process.env.MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, database) {
        if (err) {
            callback(err);
        }

        db = database.db('medit4cep').collection('patterns');

        callback(err, database);
    });
};

Patterns.prototype.add = async (pattern) => {
    return new Promise((resolve, reject) => {
        db.insertOne(pattern, (err, data) => {
            err
                ? reject(err)
                : resolve(data);
        });
    });
};

Patterns.prototype.get = async (_id) => {
    return new Promise((resolve, reject) => {
        db.find({_id: new ObjectId(_id)})
            .toArray((err, data) => {
                err
                    ? reject(err)
                    : resolve(data);
            });
    });
};

Patterns.prototype.getIds = async (ids) => {
    return new Promise((resolve, reject) => {
        db.find({_id: {$in: ids}}).toArray((err, data) => {
            err
                ? reject(err)
                : resolve(data);
        });
    });
};

Patterns.prototype.getAll = function (callback) {
    return db.find({}).toArray(callback);
};

Patterns.prototype.update = function (_id, updatedPattern, callback) {
    delete updatedPattern._id;
    return db.updateOne({_id: new ObjectId(_id)}, {$set: updatedPattern}, callback);
};

Patterns.prototype.updateDeploymentId = function (_id, deploymentId, callback) {
    return db.updateOne({_id: new ObjectId(_id)}, {
        $set: {
            deploymentId: deploymentId,
        }
    }, callback);
};

Patterns.prototype.removeAll = function (callback) {
    return db.deleteMany({}, callback);
};

async function getIds(ids) {
    return new Promise((resolve, reject) => {
        db.find({_id: {$in: ids}}).toArray((err, data) => {
            err
                ? reject(err)
                : resolve(data);
        });
    });
}

async function removePattern(_id) {
    let eventTypesService = require('./eventTypes-service');
    return new Promise(async (resolve) => {
        let pattern = await getIds([new ObjectId(_id)]);

        if (pattern.length > 0) {
            let dependencyPatterns = await getIds(pattern[0].dependencies.patternIds);
            let dependencyEventTypes = await eventTypesService.getIds(pattern[0].dependencies.eventTypeIds);

            dependencyPatterns.forEach(p => {
                let index = p.requirements.patternIds.findIndex(x => x.equals(_id));
                if (index !== -1) {
                    p.requirements.patternIds.splice(index, 1);
                    db.updateOne({_id: new ObjectId(p._id)}, {$set: p});
                }
            });

            dependencyEventTypes.forEach(eT => {
                let index = eT.patternIds.findIndex(x => x.equals(_id));
                if (index !== -1) {
                    eT.patternIds.splice(index, 1);
                    eventTypesService.update(eT._id, eT);
                }
            });

            db.deleteOne({_id: new ObjectId(_id)});

            if (pattern[0].requirements.patternIds.length > 0) {
                let promiseArr = pattern[0].requirements.patternIds.map(function (p) {
                    return removePattern(p).then(data => {
                        return data.concat([{deploymentId: pattern[0].deploymentId, pattern: pattern[0].pattern, name: pattern[0].name}]);
                    })
                });

                Promise.all(promiseArr).then(function(res){
                    resolve([...new Set(res.flatMap(x => x).map(s => JSON.stringify(s)))].map(s => JSON.parse(s)));
                }).catch(function(err){
                   console.log("Error on Promises.all", err)
                });
            } else
                resolve([{deploymentId: pattern[0].deploymentId, pattern: pattern[0].pattern, name: pattern[0].name}]);
        } else
            resolve([]);
    });
}

Patterns.prototype.removePattern = (_id) => {
    return removePattern(_id);
};

module.exports = new Patterns();
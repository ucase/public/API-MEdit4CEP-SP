'use strict';

const express = require('express');
const router = express.Router();
const patternsService = require('./patterns-service');
const eventTypesService = require('./eventTypes-service');
const ObjectId = require('mongodb').ObjectID;

router.get('/', function (req, res) {
    patternsService.getAll((err, patterns) => {
            if (err) {
                res.status(500).send({
                    msg: err
                });
            } else if (patterns !== null) {
                res.status(200).send(patterns);
            }
        }
    );
});

router.get('/:_id', async function (req, res) {
    let _id = req.params._id;
    let pattern = await patternsService.get(_id);

    if (pattern.length > 0) {
        res.status(200).send(pattern);
    } else {
        res.status(404).send({
            msg: 'Pattern not found'
        });
    }
});

router.post('/', async function (req, res) {
    let patternToAdd = req.body;

    let dependencies = patternToAdd.dependencies.map(id => new ObjectId(id));

    patternToAdd.deploymentId = patternToAdd.deploymentId ? patternToAdd.deploymentId : "null";
    patternToAdd.requirements = {eventTypeIds: [], patternIds: []};
    patternToAdd.dependencies = {eventTypeIds: [], patternIds: []};

    let dependencyEventTypes = await eventTypesService.getIds(dependencies);
    let etIds = dependencyEventTypes.map(eT => eT._id);

    let dependencyPatterns = await patternsService.getIds(dependencies);
    let pIds = dependencyPatterns.map(p => p._id);

    patternToAdd.dependencies.eventTypeIds =
        patternToAdd.dependencies.eventTypeIds.concat(etIds);
    patternToAdd.dependencies.patternIds =
        patternToAdd.dependencies.patternIds.concat(pIds);

    let patternInserted = await patternsService.add(patternToAdd);

    if ((dependencyPatterns.length + dependencyEventTypes.length) ===
        (patternToAdd.dependencies.patternIds.length + patternToAdd.dependencies.eventTypeIds.length)) {
        dependencyPatterns.forEach(p => {
            p.requirements.patternIds.push(patternInserted.ops[0]._id);
            patternsService.update(p._id, p);
        });

        dependencyEventTypes.forEach(eT => {
            eT.patternIds.push(patternInserted.ops[0]._id);
            eventTypesService.update(eT._id, eT);
        });

        res.status(201).send({
            msg: 'New pattern added!',
            id: patternInserted.ops[0]._id
        });
    } else {
        res.status(404).send({
            msg: 'One or more eventType or pattern id does not exists'
        });
    }
});

router.put('/:_id', function (req, res) {
    const _id = req.params._id;
    const updatedPattern = req.body;

    patternsService.update(_id, updatedPattern, (err, numUpdates) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (numUpdates.result.n === 0) {
            res.status(404).send({
                msg: 'Pattern not found'
            });
        } else {
            res.status(200).send({
                msg: 'Pattern updated'
            });
        }
    });
});

router.put('/:_id/deploymentId/:deploymentId', function (req, res) {
    let _id = req.params._id;
    let deploymentId = req.params.deploymentId;

    patternsService.updateDeploymentId(_id, deploymentId, (err, numUpdates) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (numUpdates.result.n === 0) {
            res.status(404).send({
                msg: 'Pattern does not exists'
            });
        } else {
            res.status(200).send({
                msg: 'Pattern updated'
            });
        }
    });
});

router.delete('/:_id', async function (req, res) {
    let _id = req.params._id;
    let pattern = [];

    if(_id != 'null')
        pattern = await patternsService.get(_id);

    if (pattern.length > 0) {
        let patternsRemoved = await patternsService.removePattern(_id);
        let patternsName = patternsRemoved.map(d => d.name);
        let patterns = patternsRemoved.map(d => d.pattern);

        res.status(200).send({
            msg: 'Pattern and requirements removed',
            deploymentId: pattern[0].deploymentId,
            patternsToRemove: [...new Set(patternsName)],
            patterns: patterns
        });
    } else {
        res.status(404).send({
            msg: 'Pattern not found',
            deploymentId: null,
            patternsToRemove: [],
            patterns: []
        });
    }
});


module.exports = router;

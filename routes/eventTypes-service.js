'use strict';

const MongoClient = require('mongodb').MongoClient;
let db;
let ObjectId = require('mongodb').ObjectID;
const Events = function () {
};

Events.prototype.connectDb = function (callback) {
    MongoClient.connect(process.env.MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, database) {
        if (err) {
            callback(err);
        }

        db = database.db('medit4cep').collection('eventTypes');

        callback(err, database);
    });
};

Events.prototype.add = function (eventType, callback) {
    return db.insertOne(eventType, callback);
};

Events.prototype.get = (_id) => {
    return new Promise((resolve, reject) => {
        db.find({_id: new ObjectId(_id)}).toArray((err, data) => {
            err
                ? reject(err)
                : resolve(data);
        });
    });
};

Events.prototype.getIds = (ids) => {
    return new Promise((resolve, reject) => {
        db.find({_id: {$in: ids}}).toArray((err, data) => {
            err
                ? reject(err)
                : resolve(data);
        });
    });
};

Events.prototype.getQuery = function (query, callback) {
    return db.find(query).toArray(callback);
};

Events.prototype.getAll = function (callback) {
    return db.find({}).toArray(callback);
};

Events.prototype.getAllDomain = function (domain, callback) {
    return db.find({domain: domain}).toArray(callback);
};

Events.prototype.update = function (_id, updatedEvent, callback) {
    delete updatedEvent._id;
    return db.updateOne({_id: new ObjectId(_id)}, {$set: updatedEvent}, callback);
};

Events.prototype.updateDeploymentId = function (_id, deploymentId, callback) {
    return db.updateOne({_id: new ObjectId(_id)}, {
        $set: {
            deploymentId: deploymentId,
        }
    }, callback);
};

Events.prototype.updateDomainName = function (eventTypeToUpdate, domainName, callback) {
    return db.updateOne(eventTypeToUpdate, {$set: {domain: domainName}}, callback);
};

Events.prototype.remove = function (_id, callback) {
    return db.deleteOne({_id: new ObjectId(_id)}, callback);
};

Events.prototype.removeAll = function (callback) {
    return db.deleteMany({}, callback);
};

Events.prototype.removeEventType = function (eventType, domainName) {
    let domainsService = require('./domains-service');
    let patternsService = require('./patterns-service');
    return new Promise(async (resolve) => {
        domainsService.pullEventTypeByName(domainName, eventType._id);
        db.deleteOne({_id: new ObjectId(eventType._id)});

        let patterns = await patternsService.getIds(eventType.patternIds);
        let patternsEPL = [];
        let patternsToRemove = [];

        for (const p of patterns){
            let removedPattern = await patternsService.removePattern(p._id);
            patternsEPL = patternsEPL.concat(removedPattern.map(d => d.pattern));
            patternsToRemove = patternsToRemove.concat(removedPattern.map(d => d.name));
        }

        resolve({deploymentId: eventType.deploymentId, patterns: patternsEPL, patternsName: patternsToRemove.concat(eventType.schema.name)});
    });
};

module.exports = new Events();
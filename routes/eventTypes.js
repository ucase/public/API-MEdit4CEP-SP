'use strict';

const express = require('express');
const router = express.Router();
const eventTypesService = require('./eventTypes-service');
const domainsService = require('./domains-service');

router.get('/', function (req, res) {
    eventTypesService.getAll((err, eventTypes) => {
            if (err) {
                res.status(500).send({
                    msg: err
                });
            } else if (eventTypes !== null) {
                res.status(200).send(eventTypes);
            }
        }
    );
});

router.get('/:_id', function (req, res) {
    let _id = req.params._id;

    eventTypesService.get(_id, (err, eventType) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (eventType.length === 0) {
            res.status(404).send(eventType);
        } else if (eventType !== null) {
            res.status(200).send(eventType);
        }
    });
});

router.get('/domain/:domainName', async (req, res) => {
    let domainName = req.params.domainName;
    let domain = await domainsService.getByName(domainName);
    
    if (domain.length > 0) {
        let eventTypeIds = domain[0].eventTypes;
        let eventTypes = await eventTypesService.getIds(eventTypeIds);

        if (eventTypes === null) {
            res.status(500).send({
                msg: err
            });
        } else if (eventTypes !== null) {
            res.status(200).send(eventTypes);
        }
    } else {
        res.status(404).send({
            msg: 'Domain not found'
        });
    }
});

router.post('/', async (req, res) => {
    let eventType = req.body;
    let domainName = eventType.domain;
    eventType.patternIds = [];

    if (domainName === "null") {
        eventTypesService.add(eventType, (err, eventType) => {
                if (err) {
                    res.status(500).send({
                        msg: err
                    });
                } else if (eventType.insertedCount === 1) {
                    res.status(201).send({
                        msg: 'New simple eventType added!',
                        id: eventType.ops[0]._id
                    });
                }
            }
        );
    } else {
        delete eventType.domain;
        let domain = await domainsService.getByName(domainName);

        if (domain.length > 0) {
            let eventTypeIds = domain[0].eventTypes;
            let eventTypes = await eventTypesService.getIds(eventTypeIds);

            if (eventTypes.some(e => e.schema.name === eventType.schema.name)) {
                res.status(409).send({
                    msg: 'A event type named ' + eventType.schema.name + ' already exists on the domain ' + domainName
                });
            } else {
                eventTypesService.add(eventType, (err, eventTypeInserted) => {
                    if (err) {
                        res.status(500).send({
                            msg: err
                        });
                    } else if (eventTypeInserted.insertedCount === 1) {
                        domainsService.addEventTypeByName(domainName, eventTypeInserted.ops[0]._id, (err, numUpdates) => {
                            if (err) {
                                res.status(500).send({
                                    msg: err
                                });
                            } else if (numUpdates.result.n === 0) {
                                res.status(404).send({
                                    msg: 'There is not a domain with the name: ' + eventType.domain
                                });
                            } else {
                                res.status(201).send({
                                    msg: 'New simple eventType added!',
                                    id: eventTypeInserted.ops[0]._id
                                });
                            }
                        });
                    } else {
                        res.status(500).send({
                            msg: 'An error occurred. Please check API logs.'
                        });
                    }
                });
            }
        } else {
            res.status(404).send({
                msg: 'There is not a domain with the name: ' + eventType.domain
            });
        }
    }
});

router.put('/:_id', function (req, res) {
    let _id = req.params._id;
    let updatedEventType = req.body;

    eventTypesService.update(_id, updatedEventType, (err, numUpdates) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (numUpdates.result.n === 0) {
            res.status(404).send({
                msg: 'EventType does not exists'
            });
        } else {
            res.status(200).send({
                msg: 'EventType updated'
            });
        }
    });
});

router.put('/:_id/deploymentId/:deploymentId', function (req, res) {
    let _id = req.params._id;
    let deploymentId = req.params.deploymentId;

    eventTypesService.updateDeploymentId(_id, deploymentId, (err, numUpdates) => {
        if (err) {
            res.status(500).send({
                msg: err
            });
        } else if (numUpdates.result.n === 0) {
            res.status(404).send({
                msg: 'EventType does not exists'
            });
        } else {
            res.status(200).send({
                msg: 'EventType updated'
            });
        }
    });
});

router.delete('/:_id/:domainName', async (req, res) => {
    let _id = req.params._id;
    let domainName = req.params.domainName;
    let eventType = [];

    if(_id != 'null')
        eventType = await eventTypesService.get(_id);

    if (eventType.length > 0) {
        let eventTypesRemoved = await eventTypesService.removeEventType(eventType[0], domainName);

        res.status(200).send({
            msg: 'EventType removed',
            deploymentId: eventType[0].deploymentId ? eventType[0].deploymentId : "",
            patternsToRemove: [...new Set(eventTypesRemoved.patternsName)],
            patterns: eventTypesRemoved.patterns
        });
    } else {
        res.status(404).send({
            msg: 'EventType not found',
            deploymentId: null,
            patternsToRemove: [],
            patterns: []
        });
    }
});


module.exports = router;
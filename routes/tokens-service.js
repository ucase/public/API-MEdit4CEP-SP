'use strict';

const MongoClient = require('mongodb').MongoClient;
let db;
const Tokens = function () {
};

Tokens.prototype.connectDb = function (callback) {
    MongoClient.connect(process.env.MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, database) {
        if (err) {
            callback(err);
        }

        db = database.db('medit4cep').collection('tokens');

        callback(err, database);
    });
};

Tokens.prototype.find = function (token, callback) {
    return db.find({token: token}).toArray(callback);
};

module.exports = new Tokens();
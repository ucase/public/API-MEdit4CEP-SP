'use strict';

const MongoClient = require('mongodb').MongoClient;
let db;
let ObjectId = require('mongodb').ObjectID;
const Domains = function () {
};

Domains.prototype.connectDb = function (callback) {
    MongoClient.connect(process.env.MONGODB_URL, {useNewUrlParser: true, useUnifiedTopology: true}, function (err, database) {
        if (err) {
            callback(err);
        }

        db = database.db('medit4cep').collection('domains');

        callback(err, database);
    });
};

Domains.prototype.add = function (domain, callback) {
    return db.updateOne({name: domain.name}, {$setOnInsert: domain}, {upsert: true}, callback);
};

Domains.prototype.get = function (_id, callback) {
    return db.find({_id: new ObjectId(_id)}).toArray(callback);
};

Domains.prototype.getByName = function (name) {
    return new Promise((resolve, reject) => {
        db.find({name: name}).toArray((err, data) => {
            err
                ? reject(err)
                : resolve(data);
        });
    });
};

Domains.prototype.getAll = function (callback) {
    return db.find({}).toArray(callback);
};

Domains.prototype.update = function (_id, updatedDomain, callback) {
    delete updatedDomain._id;
    return db.updateOne({_id: new ObjectId(_id)}, {$set: updatedDomain}, callback);
};

Domains.prototype.addEventTypeByName = function (name, eventTypeId, callback) {
    return db.updateOne({name: name}, {$addToSet: {eventTypes: new ObjectId(eventTypeId)}}, callback);
};

Domains.prototype.pullEventTypeByName = function (name, eventTypeId, callback) {
    return db.updateOne({name: name}, {$pull: {eventTypes: new ObjectId(eventTypeId)}}, callback);
};

Domains.prototype.remove = function (_id, callback) {
    return db.deleteOne({_id: new ObjectId(_id)}, callback);
};

Domains.prototype.removeAll = function (callback) {
    return db.deleteMany({}, callback);
};

module.exports = new Domains();
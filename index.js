'use strict';

const express = require('express');
const app = express();
const http = require('http');
const logger = require('morgan');
const PORT = process.env.PORT || 8080;
const bodyParser = require('body-parser');
const baseAPI = '/api/v1';
const passport = require('passport');
const Strategy = require('passport-http-bearer').Strategy;
const tokensService = require('./routes/tokens-service');
const kafka = require('kafka-node');
const client = new kafka.KafkaClient();

/*const Consumer = kafka.Consumer;
const consumer = new Consumer(client, [{
    topic: 'streams-nodejs',
    partition: 0
}], {
    autoCommit: false
});*/

passport.use(new Strategy(
    function (token, cb) {
        tokensService.find(token, function (err, token) {
            if (err) {
                return cb(err);
            }
            if (!token || token.length === 0) {
                return cb(null, false);
            }
            return cb(null, token);
        });
    }));

app.use(bodyParser.json());
app.use(logger('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));

const domainsService = require('./routes/domains-service');
const domains = require('./routes/domains');

const eventTypesService = require('./routes/eventTypes-service');
const eventTypes = require('./routes/eventTypes');

const patternsService = require('./routes/patterns-service');
const patterns = require('./routes/patterns');

app.get('/', function (req, res) {
    res.send('Welcome to MEdit4CEP API!');
});

//app.use('/', express.static(__dirname + "/public/"));

app.use(baseAPI + '/domains', passport.authenticate('bearer', {session: false}), domains);
app.use(baseAPI + '/eventTypes', passport.authenticate('bearer', {session: false}), eventTypes);
app.use(baseAPI + '/patterns', passport.authenticate('bearer', {session: false}), patterns);

const server = http.createServer(app);

const io = require('socket.io').listen(server);

io.sockets.on('connection', (socket) => {
    console.log("User connected");
});

/*consumer.on('message', function (message) {
    console.log('New action received from the SP Architecture: ', message.value);
    if (message.value.length > 37)
        io.emit('newAlert', {deploymentId: message.value.split(';')[0], code: message.value.split(';')[1]});
    else
        io.emit('newAlert', message.value);
});*/

tokensService.connectDb(function (err) {
    if (err) {
        console.log("Could not connect with MongoDB - tokensService");
        process.exit(1);
    }

    domainsService.connectDb(function (err) {
        if (err) {
            console.log("Could not connect with MongoDB - domainsService");
            process.exit(1);
        }

        eventTypesService.connectDb(function (err) {
            if (err) {
                console.log("Could not connect with MongoDB - eventTypesService");
                process.exit(1);
            }

            patternsService.connectDb(function (err) {
                if (err) {
                    console.log("Could not connect with MongoDB - patternsService");
                    process.exit(1);
                }

                server.listen(PORT, function () {
                    console.log('MEdit4CEP-SP API listening on port ' + PORT);
                });
            });
        });
    });
});

module.exports = app;
'use strict';

const chai = require('chai');
chai.use(require('chai-things'));
chai.use(require('chai-http'));
const expect = chai.expect;
const domains = require('./routes/domains-service');
const eventTypes = require('./routes/eventTypes-service');
const patterns = require('./routes/patterns-service');
const app = require('./index');

/** COMPLEX TEST RESOURCES **/

let domainsTest = [];
let eventTypesTest = [];
let patternsTest = [];

for (let i = 0; i < 3; i++) {
    domainsTest.push({
        'name': 'Test' + i,
        'description': 'Test domain' + i,
        'eventTypes': []
    });
}

for (let i = 0; i < 9; i++) {
    eventTypesTest.push({
        "schema": {
            "name": "AirQuality" + i,
            "type": "record",
            "fields": [
                {
                    "name": "p1",
                    "type": "string"
                },
                {
                    "name": "p2",
                    "type": "string"
                },
                {
                    "name": "p3",
                    "type": "double"
                },
                {
                    "name": "p4",
                    "type": "string"
                }
            ]
        },
        "deploymentId": "1258ea93-e77b-4336-8bb3-d90f73e11ca" + i,
        "domain": "null",
        "timestamp": "1549273841987",
        "patterns": []
    });
}

for (let i = 0; i < 27; i++) {
    patternsTest.push({
        "name": "Test" + i,
        "pattern": "@Name(\"Test\")  @Description(\"Test pattenr\")  @Tag(name=\"domainName\", value=\"Test\")  insert into Test  select a1.* from WaterMesurement a1",
        "timestamp": "1550487974917",
        "eventTypeIds": [],
        "deploymentId": "null"
    });
}


describe('Basic testing MEdit4CEP API', function () {

    before(function (done) {
        domains.connectDb((err) => {
            if (err) {
                return done(err);
            }

            domains.removeAll((err) => {
                if (err) {
                    return done(err);
                }
            });
        });

        eventTypes.connectDb((err) => {
            if (err) {
                return done(err);
            }

            eventTypes.removeAll((err) => {
                if (err) {
                    return done(err);
                }
            });
        });

        patterns.connectDb((err) => {
            if (err) {
                return done(err);
            }

            patterns.removeAll((err) => {
                if (err) {
                    return done(err);
                }
            });

            done();
        });
    });

    /** DOMAINS **/

    describe('/POST domain', () => {
        it('it should POST one domain', (done) => {
            chai.request(app)
                .post('/api/v1/domains')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .send({
                    'name': 'Test',
                    'description': 'Test domain',
                    'eventTypes': []
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should GET the inserted domain', (done) => {
            chai.request(app)
                .get('/api/v1/domains')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body).to.contain.an.item.with.property('name', 'Test');
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/GET domains', () => {
        it('it should GET all the domains', (done) => {
            chai.request(app)
                .get('/api/v1/domains')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/GET domain', () => {
        it('it should GET one domain', (done) => {
            chai.request(app)
                .get('/api/v1/domains/Test')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body).to.contain.an.item.with.property('name', 'Test');
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    /** EVENT TYPES **/

    let eventTypeId = "";

    describe('/POST eventType', () => {
        it('it should POST one event type', (done) => {
            chai.request(app)
                .post('/api/v1/eventTypes')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .send({
                    "schema": {
                        "name": "WaterMeasurement",
                        "type": "record",
                        "fields": [
                            {
                                "name": "p1",
                                "type": "string"
                            },
                            {
                                "name": "p2",
                                "type": "string"
                            },
                            {
                                "name": "p3",
                                "type": "double"
                            },
                            {
                                "name": "p4",
                                "type": "string"
                            }
                        ]
                    },
                    "deploymentId": "1258ea93-e77b-4336-8bb3-d90f73e11caf",
                    "domain": "null",
                    "timestamp": "1549273841987"
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    eventTypeId = res.body.id;
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should GET the inserted event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'WaterMeasurement');
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/GET eventType', () => {
        it('it should GET one event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'WaterMeasurement');
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/PUT eventType\'s domain', () => {
        it('it should add an eventTypeId to one domain', (done) => {
            chai.request(app)
                .put('/api/v1/domains/domain/Test/eventType/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should have the eventTypeId inserted', (done) => {
            chai.request(app)
                .get('/api/v1/domains/Test')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body).to.contain.an.item.with.property('name', 'Test');
                    expect(res.body[0].eventTypes).to.include.members([eventTypeId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    /** PATTERNS **/

    let patternId = "";

    describe('/POST pattern', () => {
        it('it should POST one pattern', (done) => {
            chai.request(app)
                .post('/api/v1/patterns')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .send({
                        "name": "Test",
                        "pattern": "@Name(\"Test\")  @Description(\"Test pattenr\")  @Tag(name=\"domainName\", value=\"Test\")  insert into Test  select a1.* from WaterMesurement a1",
                        "timestamp": "1550487974917",
                        "eventTypeIds": [
                            eventTypeId
                        ],
                        "deploymentId": "null"
                    }
                )
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    patternId = res.body.id;
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should GET the inserted pattern', (done) => {
            chai.request(app)
                .get('/api/v1/patterns/' + patternId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0]).to.have.property('name', 'Test');
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should exists the inserted pattern id in the event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'WaterMeasurement');
                    expect(res.body[0].patterns).to.include.members([patternId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/DELETE pattern', () => {
        it('it should DELETE the pattern', (done) => {
            chai.request(app)
                .delete('/api/v1/patterns/' + patternId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not GET the removed pattern', (done) => {
            chai.request(app)
                .get('/api/v1/patterns/' + patternId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(0);
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should GET the event type without the removed pattern id', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'WaterMeasurement');
                    expect(res.body[0].patterns).to.not.have.members([patternId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    /** EVENT TYPES **/

    describe('/DELETE eventType', () => {
        it('it should DELETE the event type', (done) => {
            chai.request(app)
                .delete('/api/v1/eventTypes/' + eventTypeId + '/Test')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not GET the removed event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(0);
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should GET the domain without the removed event type id', (done) => {
            chai.request(app)
                .get('/api/v1/domains/Test')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0]).to.have.property('name', 'Test');
                    expect(res.body[0].eventTypes).to.not.have.members([eventTypeId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    /** DOMAINS **/

    describe('/DELETE domain', () => {
        let eventTypesIds = [];
        let patternsId = [];

        before(() => {
            domains.getByName('Test', (err, domain) => {
                eventTypesIds = [...domain[0].eventTypes];

                eventTypes.getIds(eventTypesIds, (err, eventTypes) => {
                    eventTypes.forEach(eT => {
                        patternsId = patternsId.concat(eT.patterns);
                    });
                });
            });
        });

        it('it should delete the domain', (done) => {
            chai.request(app)
                .delete('/api/v1/domains/Test')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not exists any eventType that was related to the domain deleted', (done) => {
            eventTypes.getIds(eventTypesIds, (err, res) => {
                expect(res).to.have.lengthOf(0);
                done();
            });
        });

        it('it should not exists any pattern that was related to the domain deleted', (done) => {
            patterns.getIds(patternsId, (err, res) => {
                expect(res).to.have.lengthOf(0);
                done();
            });
        });
    });

    /** COMPLEX DELETING **/

    describe('/POST domain', () => {
        it('it should POST a new domain', (done) => {
            chai.request(app)
                .post('/api/v1/domains')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .send({
                    'name': 'Test New',
                    'description': 'Test domain New',
                    'eventTypes': []
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should GET the new domain', (done) => {
            chai.request(app)
                .get('/api/v1/domains/Test New')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body).to.contain.an.item.with.property('name', 'Test New');
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/POST eventType', () => {
        it('it should POST a new event type', (done) => {
            chai.request(app)
                .post('/api/v1/eventTypes')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .send({
                    "schema": {
                        "name": "AirQuality",
                        "type": "record",
                        "fields": [
                            {
                                "name": "p1",
                                "type": "string"
                            },
                            {
                                "name": "p2",
                                "type": "string"
                            },
                            {
                                "name": "p3",
                                "type": "double"
                            },
                            {
                                "name": "p4",
                                "type": "string"
                            }
                        ]
                    },
                    "deploymentId": "1258ea93-e77b-4336-8bb3-d90f73e11cff",
                    "domain": "null",
                    "timestamp": "1549273841987"
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    eventTypeId = res.body.id;
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should GET the inserted event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'AirQuality');
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/PUT eventType\'s domain', () => {
        it('it should add an eventTypeId to one domain', (done) => {
            chai.request(app)
                .put('/api/v1/domains/domain/Test New/eventType/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should have the eventTypeId inserted', (done) => {
            chai.request(app)
                .get('/api/v1/domains/Test New')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body).to.contain.an.item.with.property('name', 'Test New');
                    expect(res.body[0].eventTypes).to.include.members([eventTypeId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/POST pattern', () => {
        it('it should POST a new pattern', (done) => {
            chai.request(app)
                .post('/api/v1/patterns')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .send({
                        "name": "Test New",
                        "pattern": "@Name(\"Test New\")  @Description(\"Test pattenr\")  @Tag(name=\"domainName\", value=\"Test\")  insert into Test  select a1.* from WaterMesurement a1",
                        "timestamp": "1550487974917",
                        "eventTypeIds": [
                            eventTypeId
                        ],
                        "deploymentId": "null"
                    }
                )
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    patternId = res.body.id;
                    expect(res).to.have.status(201);
                    done();
                });
        });

        it('it should GET the inserted pattern', (done) => {
            chai.request(app)
                .get('/api/v1/patterns/' + patternId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0]).to.have.property('name', 'Test New');
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should exists the inserted pattern id in the event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypeId)
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'AirQuality');
                    expect(res.body[0].patterns).to.include.members([patternId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/DELETE domain', () => {
        let eventTypesIds = [];
        let patternsId = [];

        before(() => {
            domains.getByName('Test New', (err, domain) => {
                eventTypesIds = [...domain[0].eventTypes];

                eventTypes.getIds(eventTypesIds, (err, eventTypes) => {
                    eventTypes.forEach(eT => {
                        patternsId = patternsId.concat(eT.patterns);
                    });
                });
            });
        });

        it('it should delete the domain', (done) => {
            chai.request(app)
                .delete('/api/v1/domains/Test New')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not exists any eventType that was related to the domain deleted', (done) => {
            eventTypes.getIds(eventTypesIds, (err, res) => {
                expect(res).to.have.lengthOf(0);
                done();
            });
        });

        it('it should not exists any pattern that was related to the domain deleted', (done) => {
            patterns.getIds(patternsId, (err, res) => {
                expect(res).to.have.lengthOf(0);
                done();
            });
        });
    });

    /********** ***********/
    /** COMPLEX TESTING **/
    /********** *********/

    describe('/POST domains', () => {
        domainsTest.forEach(d => {
            it('it should POST new domains', (done) => {
                chai.request(app)
                    .post('/api/v1/domains')
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .send(d)
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res).to.have.status(201);
                        done();
                    });
            });

            it('it should GET the inserted domain', (done) => {
                chai.request(app)
                    .get('/api/v1/domains/' + d.name)
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res.body).to.have.lengthOf(1);
                        expect(res.body).to.contain.an.item.with.property('name', d.name);
                        expect(res).to.have.status(200);
                        done();
                    });
            });
        });
    });

    let eventTypesIds = [];

    describe('/POST eventTypes', () => {
        eventTypesTest.forEach((eT, i) => {
            it('it should POST new event type', (done) => {
                chai.request(app)
                    .post('/api/v1/eventTypes')
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .send(eT)
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        eventTypesIds.push(res.body.id);
                        expect(res).to.have.status(201);
                        for (let index = i * 3; index < (i * 3) + 3; index++) {
                            patternsTest[index].eventTypeIds.push(res.body.id);
                        }
                        done();
                    });
            });

            it('it should GET the inserted event type', (done) => {
                chai.request(app)
                    .get('/api/v1/eventTypes/' + eventTypesIds[i])
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res.body).to.have.lengthOf(1);
                        expect(res.body[0].schema).to.have.property('name', 'AirQuality' + i);
                        expect(res).to.have.status(200);
                        done();
                    });
            });
        });
    });

    describe('/PUT eventType\'s domain', () => {
        eventTypesTest.forEach((eT, i) => {
            it('it should add an eventTypeId to one domain', (done) => {
                chai.request(app)
                    .put('/api/v1/domains/domain/Test' + Math.floor(i / 3) + '/eventType/' + eventTypesIds[i])
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res).to.have.status(201);
                        done();
                    });
            });

            it('it should have the eventTypeId inserted', (done) => {
                chai.request(app)
                    .get('/api/v1/domains/Test' + Math.floor(i / 3))
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res.body).to.have.lengthOf(1);
                        expect(res.body).to.contain.an.item.with.property('name', 'Test' + Math.floor(i / 3));
                        expect(res.body[0].eventTypes).to.include.members([eventTypesIds[i]]);
                        expect(res).to.have.status(200);
                        done();
                    });
            });
        });
    });

    let patternsIds = [];

    describe('/POST patterns', () => {
        patternsTest.forEach((p, i) => {
            it('it should POST new pattern', (done) => {
                chai.request(app)
                    .post('/api/v1/patterns')
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .send(p)
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        patternsIds.push(res.body.id);
                        expect(res).to.have.status(201);
                        done();
                    });
            });

            it('it should GET the inserted pattern', (done) => {
                chai.request(app)
                    .get('/api/v1/patterns/' + patternsIds[i])
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res.body).to.have.lengthOf(1);
                        expect(res.body[0]).to.have.property('name', 'Test' + i);
                        expect(res).to.have.status(200);
                        done();
                    });
            });

            it('it should GET the event type with the id of the inserted pattern', (done) => {
                chai.request(app)
                    .get('/api/v1/eventTypes/' + patternsTest[i].eventTypeIds[0])
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res.body).to.have.lengthOf(1);
                        expect(res.body[0].patterns).to.include.members([patternsIds[i]]);
                        expect(res).to.have.status(200);
                        done();
                    });
            });
        });
    });

    describe('/DELETE pattern', () => {
        it('it should DELETE the pattern', (done) => {
            chai.request(app)
                .delete('/api/v1/patterns/' + patternsIds[patternsIds.length - 1])
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not GET the removed pattern', (done) => {
            chai.request(app)
                .get('/api/v1/patterns/' + patternsIds[patternsIds.length - 1])
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(0);
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should GET the event type without the removed pattern id', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypesIds[eventTypesIds.length - 1])
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].schema).to.have.property('name', 'AirQuality8');
                    expect(res.body[0].patterns).to.not.have.members([patternId]);
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('/DELETE eventType', () => {
        it('it should DELETE the event type', (done) => {
            chai.request(app)
                .delete('/api/v1/eventTypes/' + eventTypesIds[eventTypesIds.length - 1] + '/Test2')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not GET the removed event type', (done) => {
            chai.request(app)
                .get('/api/v1/eventTypes/' + eventTypesIds[eventTypesIds.length - 1])
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(0);
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should GET the domain without the removed event type id', (done) => {
            chai.request(app)
                .get('/api/v1/domains/Test2')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0]).to.have.property('name', 'Test2');
                    expect(res.body[0].eventTypes).to.not.have.members([eventTypesIds[eventTypesIds.length - 1]]);
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not GET the patterns that depends on the deleted event type', (done) => {
            for (let i = 24; i <= 25; i++) {
                chai.request(app)
                    .get('/api/v1/patterns/' + patternsIds[i])
                    .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                    .end((err, res) => {
                        if (err) {
                            return done(err);
                        }
                        expect(res.body).to.have.lengthOf(0);
                        expect(res).to.have.status(404);
                    });
            }
            done();
        });
    });

    describe('/DELETE domain', () => {
        let eventTypesIds = [];
        let patternsId = [];

        before(() => {
            domains.getByName('Test2', (err, domain) => {
                eventTypesIds = [...domain[0].eventTypes];

                eventTypes.getIds(eventTypesIds, (err, eventTypes) => {
                    eventTypes.forEach(eT => {
                        patternsId = patternsId.concat(eT.patterns);
                    });
                });
            });
        });

        it('it should delete the domain', (done) => {
            chai.request(app)
                .delete('/api/v1/domains/Test2')
                .set('Authorization', 'Bearer @Kw6vn&vpeBW0lP*J*@F9Ofw3Vu2')
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    expect(res).to.have.status(200);
                    done();
                });
        });

        it('it should not exists any eventType that was related to the domain deleted', (done) => {
            eventTypes.getIds(eventTypesIds, (err, res) => {
                expect(res).to.have.lengthOf(0);
                done();
            });
        });

        it('it should not exists any pattern that was related to the domain deleted', (done) => {
            setTimeout(() => {
            }, 5000);
            patterns.getIds(patternsId, (err, res) => {
                expect(res).to.have.lengthOf(0);
                done();
            });
        });
    });

    after(function (done) {
        domains.connectDb((err) => {
            if (err) {
                return done(err);
            }

            domains.removeAll((err) => {
                if (err) {
                    return done(err);
                }
            });
        });

        eventTypes.connectDb((err) => {
            if (err) {
                return done(err);
            }

            eventTypes.removeAll((err) => {
                if (err) {
                    return done(err);
                }
            });
        });

        patterns.connectDb((err) => {
            if (err) {
                return done(err);
            }

            patterns.removeAll((err) => {
                if (err) {
                    return done(err);
                }
            });

            done();
        });
    });
});

/*
let eventType = {
    "_id": {
        "$oid": "5c6a9153fb6fc01c4ce6a2ec"
    },
    "schema": {
        "name": "WaterMesurement",
        "type": "record",
        "fields": [
            {
                "name": "p1",
                "type": "string"
            },
            {
                "name": "p2",
                "type": "string"
            },
            {
                "name": "p3",
                "type": "double"
            },
            {
                "name": "p4",
                "type": "string"
            }
        ]
    },
    "deploymentId": "1258ea93-e77b-4336-8bb3-d90f73e11caf",
    "domain": "null",
    "timestamp": "1549273841987",
    "patterns": [
        {
            "$oid": "5c6a91a70ba8523f78332c01"
        }
    ]
};

let pattern = {
    "_id": {
        "$oid": "5c6a91a70ba8523f78332c01"
    },
    "name": "Test",
    "pattern": "@Name(\"Test\")  @Description(\"Test pattenr\")  @Tag(name=\"domainName\", value=\"Test\")  insert into Test  select a1.* from WaterMesurement a1",
    "timestamp": "1550487974917",
    "eventTypeIds": [
        {
            "$oid": "5c6a9153fb6fc01c4ce6a2ec"
        }
    ],
    "deploymentId": "null"
};

let domain = {
    "_id": {
        "$oid": "5c6a915d1bbe137d70174ed5"
    },
    "name": "Test",
    "description": "Test domain",
    "eventTypes": [
        {
            "$oid": "5c6a9153fb6fc01c4ce6a2ec"
        }
    ]
};
*/
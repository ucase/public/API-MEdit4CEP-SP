# API MEdit4CEP-SP
This is the RESTful API developed along MEdit4CEP-SP [[1](https://gitlab.com/ucase/public/MEdit4CEP-SP)], so it can provide persitence and another capabilities to the new extension of the graphical editor.

### Prerequisites
- Internet connection.
- NodeJs and npm installed, check it writing `node -v` and `npm -v`. You download them from [[2](https://nodejs.org/)].

## How to run this API
1. Clone the repository.
2. Install dependencies with the command `npm install` in the repository's folder
3. Configure the enviroment variable of the MongoDB database: 
	i. Windows: `set MONGODB_URL=<your_mongodb_uri>`
	ii. Linux & Mac: `export MONGODB_URL=<your_mongodb_uri>`
4. Run the API with the command `npm run start`.

Your API is already listening and ready to work in sync with the SP Architecture [[3](https://gitlab.com/ucase/public/sp-architecture)] and MEdit4CEP-SP  [[1](https://gitlab.com/ucase/public/MEdit4CEP-SP)].
angular.module("AlertListApp").controller("AlertCtrl", function ($scope, $http, $location, $q) {
    
    $scope.deployments = [];

    $scope.undeployments = [];

    let socket = io();
    socket.on('connect', function () {
        console.log("Connected to socket: " + socket.id);
    });

    socket.on('newAlert', function (data) {
        console.log("Data: ", data);
        
        if(data.deploymentId)
            $scope.deployments.push(data)
         else {
            $scope.undeployments.push($scope.deployments.filter(d => d.deploymentId === data)[0]);
            $scope.deployments = $scope.deployments.filter(d => d.deploymentId != data);
        }

        console.log('Deployments: ', $scope.deployments);
        console.log('Uneployments: ', $scope.undeployments);
        $scope.$apply();
    });

    function refresh() {
        console.log("Refreshing");
    }

    $scope.refresh = function () {
        refresh();
    };

    refresh();
});